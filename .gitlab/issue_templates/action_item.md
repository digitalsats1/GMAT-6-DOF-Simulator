---
Meeting Date: ''
Attendees: ''
Title: 'Title: '
Labels: 'Action Item (from internal meeting)'
Potential Owners: '@DigitSats; @goldharv; @kulaih; @jkkastner; @supalogix; @jabberwockie; @jimst_' 

---

**Describe the action item**
- fill in:
    - the meeting date 
    - and attendees
- in 'Labels' field, select the given label name to this issue
- in 'Assignees' field, select the potential owners listed above


